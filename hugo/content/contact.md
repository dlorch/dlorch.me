---
title: Contact
description: How to get in touch with me
---

**Daniel Lorch**  
Viktoriastrasse 21  
CH-3013 Bern  
Switzerland  
dlorch@gmail.com

DevOps Consultant with focus  
on cloud native technologies  
(e.g. Docker, Kubernetes) and  
Continuous Integration/  
Continuous Delivery - CI/CD.

I work for @AWSCloud &  
opinions are my own.
