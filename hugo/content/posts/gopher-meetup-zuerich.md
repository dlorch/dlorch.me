---
title: "Gopher Meetup Zürich"
date: 2020-02-25
tags: [gopher, meetup, zürich, swisscom] 
---

It was a pleasure to host the Zürich Gophers Meetup at Swisscom.
Find event photos here: https://www.meetup.com/de-DE/Zurich-Gophers/events/267144500/
