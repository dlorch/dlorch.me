---
title: "Mechanische Tastatur mit Schweizerdeutscher Belegung für Macs"
date: 2019-11-17
tags: [mechanical-keyboards, ch-de, wasd, mac, apple]
---

![WASD Mechanische Tastatur mit Scheizerdeutscher Belegung für Macs](/images/wasd-chde-mac-lorch.jpg)

Primäres Unterscheidungsmerkmal bei Tastaturen sind die verwendeten Schalter
(engl. Switches), die bei den einzelnen Tasten zum Einsatz kommen. Bei mechanischen
Tastaturen sind diese Schalter eben mechanisch und ermöglichen angenehmes Schreiben 
mit spürbarem Widerstand und akustischer Rückmeldung beim Tastendruck.

Mein Wunsch war es eine mechanische Tastatur für meinen Mac mit Schweizerdeutscher
Belegung (ä, ö, ü) zu haben. Da ich diese in meiner gewünschten Ausstattung nicht
von der Stange finden konnte, habe ich mir selbst eine zusammengestellt. Die
gesammelten Vorlagen und Konfigurationsdateien habe ich öffentlich zugänglich
unter folgender Adresse abgelegt:

https://github.com/dlorch/mechanical-keyboard-swiss-layout-mac

#### Allgemeine Informationen zu mechanischen Tastaturen

Im Folgenden werde ich englische Begriffe mit deutschem Text mischen, da es sonst
etwas unnatürlich wäre, wenn ich die gängigen Fachbegriffe ins Deutsch übersetzen
müsste.

**Switches** sind lassen sich über folgende Eigenschaften charakterisieren:

* **Clicky**: gibt es einen hörbaren Klick beim Tastendruck?
* **Tactile**: gibt es beim Herunterdrücken der Taste einen spürbaren "Höcker", bevor
  die Taste auslöst? Dann handelt es sich um Tactile Switches.
* **Linear**: ist der Widerstand beim Herunterdrücken der Taste auf dem ganzen Weg gleichmässig?
  Dann handelte sich um Linear Switches.
* **Actuation Force**: wieviel Kraft muss zum Herunterdrücken der Taste aufgewendet werden?
  Angabe in Gramm.
* **Actuation Point**: auf welcher Wegstrecke beim Herunterdrücken der Taste wird diese ausgelöst?

Switches werden von unterschiedlichen Herstellern gebaut. Die bekanntesten Switches sind
die von [Cherry MX]. Andere Hersteller haben ihre eigenen Switches, z.B. [Logitech Romer-G]
oder [Razer Mechanical Switches]. Nicht alle Hersteller haben ihre eigenen Switches - häufig
werden die Switches von Cherry MX oder Kopien davon verbaut.

Die Switches sind über ihre Farbe gekennzeichnet. Leider sind die Farbkodierungen von
Hersteller zu Hersteller unterschiedlich, was zunächst etwas verwirrend sein kann. Auf
der folgenden Seite gibt es eine Übersicht der Farbcodierungen für Cherry MX Switches
und deren Eigenschaften:

https://mechanicalkeyboards.com/faqs.php?q=mechanical_switch_difference

In jedem Fall lohnt es sich, sich zunächst zu überlegen, welche Eigenschaften man möchte
und sich in einem zweiten Schritt die passenden Produkte zu suchen. Welche Switches sind
die passenden für dich? Das Video [Which Cherry MX Key to use? | BeatTheBush] gibt eine
gute Übersicht. Ich habe mir vor dem Kauf meiner Tastatur den [WASD 9-Key Cherry MX Switch Tester]
erworben, um die Geräuschentwicklung zu testen. Zum Testen der Geräusche ist der Tester jedoch
nicht geeignet und dafür würde ich ihn nicht empfehlen. Der Tastendruck löst zwei Geräusche aus:

* der erste Klick-Ton entsteht beim Erreichen des "Höckers" (dem eigentlichen Tastaturanschlag),
* der zweite Ton beim Erreichen des "Bodens" der Tastatur. Bei diesem zweiten Ton spielt das Volumen
und die Konstruktion des Tastaturkörpers eine wesentliche Rolle, welche beim Tester anders ausfällt als bei der richtigen Tastatur.

Für den Einsatz im Grossraumbüro habe ich mich für die Cherry MX Brown (Tactile) ohne O-Ringe
entschieden und bin damit bisher gut gefahren. Mein Modell mit Vorlagen und Konfigurationsdateien
sind hier auffindbar:

https://github.com/dlorch/mechanical-keyboard-swiss-layout-mac

[Cherry MX]: https://www.cherrymx.de/
[Logitech Romer-G]: https://www.logitechg.com/
[Razer Mechanical Switches]: https://www.razer.com/
[Which Cherry MX Key to use? | BeatTheBush]: https://www.youtube.com/watch?v=1WYWePNJTo4
[WASD 9-Key Cherry MX Switch Tester]: https://www.wasdkeyboards.com/wasd-9-key-cherry-mx-switch-tester.html
