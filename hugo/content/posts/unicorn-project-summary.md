---
title: "Summary: The Unicorn Project (Gene Kim, 2019)"
date: 2020-07-04
tags: [devops,book,summary,genekim]
---

Learn about the «Five Ideals» from Gene Kim's book «The Unicorn Project» -
Key Points in 6 Minutes: https://www.linkedin.com/feed/update/urn:li:activity:6685427001769111552/

{{< linkedin id="6685426702149029888" height="900" >}}
