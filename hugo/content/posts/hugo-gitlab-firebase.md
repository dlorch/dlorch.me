---
title: "Automatically deploy a Hugo website to Firebase using Gitlab CI"
date: 2019-08-13
tags: [hugo, gitlab, firebase]
---

This page is generated using the [Hugo] static page generator. Every time I push
content updates to the [Gitlab] repository, a build pipeline is triggered using
Gitlab's Continuous Integration (CI) pipeline feature, and static content is
pushed to [Firebase] to be served on [dlorch.me].

These are my notes on how I've set up the build pipeline in Gitlab CI to
accomplish this task. First, obtain a CI login token from Firebase for your
website. A browser will pop up to authenticate the Firebase CLI tool, then
a token will be returned:

```shell
$ firebase login:ci

Visit this URL on any device to log in:
https://accounts.google.com/o/oauth2/auth?client_id=[..]

Waiting for authentication...

✔  Success! Use this token to login on a CI server:

[................................................]

Example: firebase deploy --token "$FIREBASE_TOKEN"
```

Go to the "Settings" page of your Git repository on Gitlab, then to "CI/CD" and
"Variables". Create two variables:

* `FIREBASE_TOKEN` (masked) with the value obtained above
* `FIREBASE_PROJECT_ID` with the project id on Firebase (use `firebase list` to find the project ID)

Then add a `.gitlab-ci.yaml` to your repository with the following contents:

```yaml
stages:
  - build
  - deploy

hugo-build:
  image: registry.gitlab.com/pages/hugo:latest
  stage: build
  only:
    - master
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  artifacts:
    paths:
      - public/
  script:
    - hugo

firebase-deploy:
  image: node:8-alpine
  stage: deploy
  only:
    - master
  script:
    - npm install -g firebase-tools
    - firebase deploy --token "$FIREBASE_TOKEN" -P "$FIREBASE_PROJECT_ID"
```

Push to Gitlab, watch how the pipeline executes and deploys the website.

[Hugo]: https://gohugo.io/
[Gitlab]: https://about.gitlab.com
[Firebase]: https://firebase.google.com/
[dlorch.me]: https://dlorch.me
