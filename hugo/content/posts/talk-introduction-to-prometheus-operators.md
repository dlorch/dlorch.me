---
title: "Talk: Introduction to Prometheus Operators (TechTalkThursday 9)"
date: 2020-03-12
tags: [talk,prometheus,operators,techtalkthursday]
---

Watch the talk:

{{< youtube 80g6Ce9qhxE >}}

Link to slides and code: https://github.com/dlorch/presentations/tree/master/2020-introduction-prometheus-operators-techtalkthursday-nine
