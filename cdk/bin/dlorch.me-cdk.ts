#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { DlorchMeCdkStack } from '../lib/dlorch.me-cdk-stack';

const app = new cdk.App();
new DlorchMeCdkStack(app, 'DlorchMeCdkStack');
