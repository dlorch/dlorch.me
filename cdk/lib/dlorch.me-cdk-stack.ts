import * as cdk from '@aws-cdk/core';
import * as acm from '@aws-cdk/aws-certificatemanager'
import * as route53 from '@aws-cdk/aws-route53'
import * as s3 from '@aws-cdk/aws-s3'
import * as s3deploy from '@aws-cdk/aws-s3-deployment'
import * as cloudfront from '@aws-cdk/aws-cloudfront'
import * as targets from '@aws-cdk/aws-route53-targets/lib'
export class DlorchMeCdkStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    /**
     * Serve static site on https://dlorch.me/
     * Setup: Route53 -> Certificate Manager + CloudFront -> S3
     * Cloudfront is required for HTTPS (S3 website hosting only supports HTTP)
     * Loosely based on https://github.com/aws-samples/aws-cdk-examples/blob/master/typescript/static-site/static-site.ts
     */

    const dlorchMeZone = new route53.HostedZone(this, 'dlorchMeZone', {
      zoneName: 'dlorch.me',
    });

    const dlorchMeCertificate = new acm.DnsValidatedCertificate(this, 'dlorchMeCertificate', {
      domainName: 'dlorch.me',
      subjectAlternativeNames: ['www.dlorch.me'],
      hostedZone: dlorchMeZone,
      region: 'us-east-1', // Cloudfront expects certificate to be in this region
    });

    const dlorchMeBucket = new s3.Bucket(this, 'dlorchMeBucket', {
      bucketName: 'dlorch.me',
      publicReadAccess: true,
      websiteIndexDocument: "index.html",
      websiteErrorDocument: "404.html",
    });

    const dlorchMeDistribution = new cloudfront.CloudFrontWebDistribution(this, 'dlorchMeWebDistribution', {
      aliasConfiguration: {
        acmCertRef: dlorchMeCertificate.certificateArn,
        names: [ 'dlorch.me' ],
        sslMethod: cloudfront.SSLMethod.SNI,
        securityPolicy: cloudfront.SecurityPolicyProtocol.TLS_V1_2_2019,
      },
      defaultRootObject: '',
      originConfigs: [
        {
          customOriginSource: {
            domainName: dlorchMeBucket.bucketWebsiteDomainName,
            originProtocolPolicy: cloudfront.OriginProtocolPolicy.HTTP_ONLY,
          },
          behaviors: [ {isDefaultBehavior: true} ],
        }
      ]
    });

    new route53.ARecord(this, 'dlorchMeARecord', {
      zone: dlorchMeZone,
      recordName: 'dlorch.me',
      target: route53.RecordTarget.fromAlias(new targets.CloudFrontTarget(dlorchMeDistribution)),
    });

    new s3deploy.BucketDeployment(this, 'dlorchMeBucketDeployment', {
      sources: [ s3deploy.Source.asset('../hugo/public') ],
      destinationBucket: dlorchMeBucket,
      distribution: dlorchMeDistribution,
      distributionPaths: ['/*'],
    });

    /**
     * Redirect https://www.dlorch.me/ to https://dlorch.me/
     * Setup: Route53 -> CloudFront -> S3
     * The easiest way for the redirect is an empty S3 bucket with redirect rule + cloudfront for HTTPS
     */

    const dlorchMeWwwBucket = new s3.Bucket(this, 'dlorchMeWwwBucket', {
      bucketName: 'www.dlorch.me',
      websiteRedirect: {
        protocol: s3.RedirectProtocol.HTTPS,
        hostName: 'dlorch.me',
      },
    });

    const dlorchMeWwwDistribution = new cloudfront.CloudFrontWebDistribution(this, 'dlorchMeWwwWebDistribution', {
      aliasConfiguration: {
        acmCertRef: dlorchMeCertificate.certificateArn,
        names: [ 'www.dlorch.me' ],
        sslMethod: cloudfront.SSLMethod.SNI,
        securityPolicy: cloudfront.SecurityPolicyProtocol.TLS_V1_2_2019,
      },
      defaultRootObject: '',
      originConfigs: [
        {
          customOriginSource: {
            domainName: dlorchMeWwwBucket.bucketWebsiteDomainName,
            originProtocolPolicy: cloudfront.OriginProtocolPolicy.HTTP_ONLY,
          },
          behaviors: [ {isDefaultBehavior: true} ],
        }
      ]
    });

    new route53.ARecord(this, 'dlorchMeARecordWWW', {
      zone: dlorchMeZone,
      recordName: 'www.dlorch.me',
      target: route53.RecordTarget.fromAlias(new targets.CloudFrontTarget(dlorchMeWwwDistribution)),
    });

    new cdk.CfnOutput(this, 'Site', { value: 'https://dlorch.me/' });
  }
}