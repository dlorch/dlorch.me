dlorch.me personal website
==========================

Services and Tools used:
* Amazon S3
* Amazon CloudFront
* AWS Certificate Manager
* AWS Cloud Development Kit (AWS CDK)
* AWS CloudFormation
* Amazon Route 53
* Hugo static website engine

Relevant files:
* GitLab pipeline: [.gitlab-ci.yml](.gitlab-ci.yml)
* CDK configuration: [dlorch.me-cdk-stack.ts](cdk/lib/dlorch.me-cdk-stack.ts)
* Hugo configuration: [config.toml](hugo/config.toml)

![CloudFormation screenshot](https://gitlab.com/dlorch/dlorch.me/-/raw/master/assets/cloudformation.png)
